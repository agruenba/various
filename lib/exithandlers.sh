# only run once
! type -t add_exit_handler > /dev/null || return 0

declare -a exit_handlers

add_exit_handler()
{
	exit_handlers[${#exit_handlers[@]}]=$1
}

remove_exit_handler()
{
	# local -a handlers # ?
	declare -a handlers
	local h
	for h in "${exit_handlers[@]}"
	do
		[ "$h" = "$1" ] && continue
		handlers[${#handlers[@]}]=$h
	done
	exit_handlers=( "${handlers[@]}" )
}

run_exit_handlers()
{
	local h
	for h in "${exit_handlers[@]}"
	do
		eval $h
	done
}

trap run_exit_handlers EXIT
