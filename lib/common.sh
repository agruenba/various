# only run once
! type -t cluster_nodes > /dev/null || return 0

source $lib/exithandlers.sh

cluster_nodes() {
    local resource_file=$1

    set -- $(xmlstarlet sel -t \
	-m sts-resource/cluster/clusternodes/clusternode \
	-v @name -n \
	"$resource_file") || exit
    if [ $# -eq 0 ]; then
	printf "No cluster nodes found in %s\n" "$resource_file"
	exit 2
    fi
    echo "$@"
}

driver_for_nodes() {
    local -A drivers
    local node

    for node in "$@"; do
	case "$node" in
	    # bob-rhel9 | bob-upstream9 | bob-upstream | bob-xfstests
	    bob9-*)
		drivers["bob-driver9"]=1
		;;
	    bob-*)
		drivers["bob-driver"]=1
		;;
	    bob7-* | node7-*)
		drivers["bob7-driver"]=1
		;;
	    *)
		echo "No driver defined for node $node" >&2
		return 2
		;;
	esac
    done

    if [ ${#drivers[@]} -gt 1 ]; then
	echo "Multiple drivers for nodes $*" >&2
	return 2
    fi

    echo "${!drivers[@]}"
}

determine_nodes_and_driver() {
    local resource_file=$1

    nodes=( $(cluster_nodes "$resource_file") ) || return
    echo "Cluster nodes: ${nodes[*]}"

    if [ -z "$driver_node" ]; then
	driver_node="$(driver_for_nodes "${nodes[@]}")" || return
    fi
    echo "Driver node: $driver_node"
}

capture_consoles() {
    local logdir="$1"
    shift

    "$lib/tail-console-logs" -C "$logdir" -- "$@" &
    add_exit_handler "kill -TERM $! > /dev/null 2>&1 || :"
    sleep 1
}

keep_status() {
    local status=$?
    "$@" || :
    return $status
}

elapsed() {
    local start=$1

    local elapsed=$(( $(date +%s) - start ))
    date "+$((elapsed / 3600)):%M:%S" -u --date="@$elapsed"
}

report_status() {
    local status=$? start=$1

    echo "Status: $status"
    echo "Runtime: $(elapsed "$start")"
    return $status
}

log_kernel_versions() {
    local -a c=()
    for node in "${nodes[@]}"; do
	ssh "$node" 'echo "kernel version $(uname -r)" > /dev/kmsg' &
	c+=($!)
    done
    wait "${c[@]}"
}
