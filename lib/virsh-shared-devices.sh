declare -A shared_host_devices=()

__find_shared_devices() {
    local node hostdev guestdev n

    for node in "${nodes[@]}"; do
	while read hostdev guestdev; do
	    shared_host_devices[$hostdev]+=" $guestdev"
	done < <( \
	    virsh dumpxml "$node" \
	    | xmlstarlet sel \
		-t -m "domain/devices/disk[@type='file']" \
		    -v source/@file -o ' /dev/' -v target/@dev -n \
		-t -m "domain/devices/disk[@type='block']" \
		    -v source/@dev -o ' /dev/' -v target/@dev -n
	)
    done

    for hostdev in "${!shared_host_devices[@]}"; do
	set -- ${shared_host_devices[$hostdev]}
	if [ $# -ne ${#nodes[@]} ]; then
	    unset shared_host_devices[$hostdev]
	fi
    done
}

# Expects ${nodes[@]} to be set to the list of cluster nodes.
shared_host_devices() {
    [ -v shared_host_devices ] || __find_shared_devices

    printf "%s\n" "${!shared_host_devices[@]}"
}

# Contruct a STS_USABLE_DISKS_OVERRIDE value of the form:
#   node1:/dev/vdf,/dev/vdi;node2:/dev/vdf,/dev/vdk;node3:/dev/vdf,/dev/vdj
#
# Expects ${nodes[@]} to be set to the list of cluster nodes.
usable_disks_override() {
    local devices=("$@")
    local -A shared_node_devices=()
    local hostdev node nodedevices

    [ -v shared_host_devices ] || __find_shared_devices

    if [ ${#devices[@]} -eq 0 ]; then
	devices=("${!shared_host_devices[@]}")
    fi

    for hostdev in "${devices[@]}"; do
	set -- ${shared_host_devices[$hostdev]}
	if [ $# -eq 0 ]; then
	    echo "Device $hostdev not shared among nodes ${nodes[*]}" >&2
	    return 1
	fi

	for ((n=1; n<=$#; n++)); do
	    node=${nodes[n-1]}
	    shared_node_devices[$node]+=" ${!n}"
	done
    done

    local str
    for node in "${nodes[@]}"; do
	nodedevices=${shared_node_devices[$node]# }
	str="$str;$node:${nodedevices// /,}"
    done
    echo ${str#;}
}

#shared_host_devices
#usable_disks_override
