#!/bin/bash

# The resources must be disabled by "pcs resource disable $(seq -f bobvirt%g 1 59)" or similar first.
# Uses "vgchange --lock-start" for lvmlockd, which isn't used befor rhel-9.

GLOB=fsck/bobvirt*
MKFS_OPTS=(-j5)

set -e

glob() {
    local glob=$1

    while read lv; do
        case "$lv" in
        $glob)
            echo "$lv"
            ;;
        esac
    done
}

vgchange --lock-start "${GLOB%%/*}"
lvs=( $(lvs -o lv_full_name | glob "$GLOB") )
for lv in "${lvs[@]}"; do
   echo "$lv"
   lvchange -ay "$lv"
   mkfs.gfs2 -q -O "${MKFS_OPTS[@]}" -plock_dlm -t "bobvirt:${lv#*/}" /dev/$lv
   lvchange -an "$lv"
done
vgchange --lock-stop "${GLOB%%/*}"
